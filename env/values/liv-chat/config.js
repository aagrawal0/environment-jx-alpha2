'use strict';
module.exports = {
    environment: 'local',
    db: {
        "username": "livspace",
        "password": "Livspace123Stage",
        "database": "liv_chat",
        "host": "livspace-db",
        "dialect": "mysql",
        "timezone" : "+05:30",
        "logging": false
    },
    base_url: "http://chat.beta.livspace.com",
    port : 8000,
    chat_host_domain : 'livspace.com',
    canvas_host : 'http://canvas.beta.livspace.com',
    website_host : 'http://livspace-web.beta.livspace.com',
    notification_retry_after : 15,  // minutes ( fractions not allowed )
    email_unread_notification_after : 1,  // minutes ( fractions not allowed )
    bouncer_cache_refresh_after: 720, // twice per day
    ls_bot : {
        "email" : "ls.bot@livspace.com",
        "name" : "LivSpace",
        "avtar" : "http://imgs.livspace.com/ls_image/419048/logoicon.png"
    },
    log_level : "debug",
    log_dir : "/var/log/liv-chat/",
    loggers: ["sysout"],
    bouncer: {
        host: "bouncer.beta.livspace.com",
        headers : {
            "X-CLIENT-ID":"CHAT",
            "X-CLIENT-SECRET":"f4b7a358-2725-11e6-b67b-9e71128bgf77",
            "Content-type": 'application/json'
        },
        app: 'CHAT',
        secret: "f4b7a358-2725-11e6-b67b-9e71128qwe77"
    },
    lpsaas: {
        host: "launchpad-backend",
        headers : {
            "X-CLIENT-ID":"CHAT",
            "X-CLIENT-SECRET":"f4b7a358-2725-11e6-b67b-9e71128bgf77",
            "Content-type": 'application/json'
        },
        app: 'CHAT',
        secret: "f4b7a358-2725-11e6-b67b-9e71128qwe77"
    },
    amqp: {
        host: "rabbitmq",
        port: "5672",
        user: "livspace",
        password: "LivspaceRandom123",
        exchange: "livspace-star-ms",
        queue: "chat"
    },
    sentry: {
        dsn : "https://00ada70d97544ac4be9357948bc24dfc@sentry.livspace.com/45",
        env: "alpha2"
    },
    apm: {
        serviceName: 'liv-chat',
        secretToken: '',
        serverUrl: "http://apm-server.livspace.com"
     }
}
