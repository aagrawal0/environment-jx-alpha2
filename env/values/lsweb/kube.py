from .base import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'k^j3ora(dh1ij^_sd)o+el)wqic=(rsk^sg@t#q^be5&y$_-_)'

# SECURITY WARNING: define the correct hosts in production!
ALLOWED_HOSTS = ['*'] 

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'lsweb_alpha2',
        'USER': 'livspace',
        'PASSWORD': 'livspaceadmin',
        'HOST': 'wcms-db',
        'PORT': '3306',
    }
}

BOUNCER = {
    'HOST': 'bouncer.alpha2.livspace.com',
    'HEADERS': '{"X-CLIENT-ID":"LAUNCHPAD","X-CLIENT-SECRET":"6ee38134-64c0-4095-af8a-38f05e69b0bd"}',
    'id': 20,
    'app': 'LAUNCHPAD',
    'secret': '6ee38134-64c0-4095-af8a-38f05e69b0bd',
    'COOKIE_NAME': 'B',
    'EMPLOYEE_COOKIE_KEY': '770A8A65DA156D24EE2A093277530142',
    'REQUESTER_TOKEN_KEY': '770A8A65DA156D24EE2A093277530142',
    'COOKIE_DOMAIN': 'lpdev.livspace.com',
    'COOKIE_PATH': '/',
    'REFRESH_TIME_IN_MINUTE': 30,
    'COOKIE_MAX_AGE': 3600,
}

LAUNCHPAD_URL = "launchpad-backend"
LIVHOME_URL = "livhome-backend"
TOOL_HOST = "http://livhome"
TOOL_AUTH_TOKEN = "30a42e59-1c05-4faf-b309-45aaece28015"
WCMS_URL = "wcms"
# LAUNCHPAD_URL = 'launchpad-backend.livspace.com'
# LIVHOME_URL = 'livhome-backend.livspace.com'

# Have to add correct values
URI = "http://lsweb.alpha2.livspace.com"

try:
    from .local import *
except ImportError:
    pass

USE_TZ = True

ENV = 'Dev'
GRAPPELLI_ADMIN_TITLE = 'Livspace Web Admin (alpha2)'


JWT = {
    'JWT_SECRET': 'ad827fe3-f558-4fc3-b780-6a3a86ef3e38',
    'JWT_EXPIRATION_DELTA': datetime.timedelta(seconds=259200),
    'JWT_ALGORITHM': 'HS256',
    'JWT_VERIFY': True,
    'JWT_VERIFY_EXPIRATION': True,

    'JWT_ALLOW_REFRESH': False,
    'JWT_REFRESH_EXPIRATION_DELTA': datetime.timedelta(seconds=259200),

    'JWT_AUTH_HEADER_PREFIX': 'Bearer',
}

JWT_AUTH_ENABLED = True
APPLICATION_SECRET = '5d0d736e-fd2a-49f7-9b7d-99eb061e37a7'


# AWS s3 bucket settings

STATICFILES_STORAGE = 'storages.backends.s3boto3.S3Boto3Storage'
AWS_DEFAULT_ACL = 'public-read'
AWS_ACCESS_KEY_ID = 'AKIARPQNMXYNPB5GOP5R'
AWS_SECRET_ACCESS_KEY = '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'
AWS_STORAGE_BUCKET_NAME = 'livspace.website.test'
# AWS_S3_CUSTOM_DOMAIN = '%s.s3.amazonaws.com' % AWS_STORAGE_BUCKET_NAME
AWS_S3_CUSTOM_DOMAIN = 'd1z63ui8y2ntmx.cloudfront.net'
AWS_S3_OBJECT_PARAMETERS = {
    'CacheControl': 'max-age=86400',
}

# Elastic Search Configuration
AWS_ES_HOST_REGION = 'ap-southeast-1'
AWS_ES_HOST = 'search-lsweb-nkfgkbcz7bcekepwutkfr2ftga.ap-southeast-1.es.amazonaws.com'
AWS_ELASTIC_ACCESS_KEY = 'AKIARPQNMXYNPB5GOP5R'
AWS_ELASTIC_SECRET_KEY = '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'

EVENT_SYSTEM_ENVIRONMENT = 'local'

ES_PROPERTY_INDEX = 'alpha2_property_index'
ES_LOOK_INDEX = 'alpha2_look_index'

# Google API Config
GOOGLE_MAP_HOST = 'maps.googleapis.com/maps/api'
GOOGLE_MAP_API_KEY = 'AIzaSyAN85tzrovp9RrvB0ZGoYrd67QnO7k0pUU'

# Max File Upload size
DATA_UPLOAD_MAX_MEMORY_SIZE = 524288000

# Logging Properties

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'simple': {
            'format': '{asctime} {levelname} {module} {filename} {lineno} - {message}',
            'style': '{',
        },
    },
    'handlers': {
        'console': {
            'level': 'INFO',
            'class': 'logging.StreamHandler',
            'formatter': 'simple'
        },
    },
    'loggers': {
        'django': {
            'handlers': ['console'],
            'propagate': True,
        },
        'application': {
            'handlers': ['console'],
            'propagate': True,
        },
        'root': {
            'handlers': ['console'],
            'propagate': True,
        },
    },
}
