CONFIG = {
    "DJANGO_LOG_LEVEL": "INFO",
    "DATABASE": {
        "HOST": "launchpad-postgres",
        "USERNAME": "postgres",
        "PASSWORD": "livspaceadmin",
        "DB_NAME": "kamar-taj-alpha2",
        "PORT": "5432",
    },
    "DEBUG": False,
    "ALLOWED_HOSTS": ["*"],
    "EVENT_SERVICE": {
        "environment": "local",
        "system_name": "kamar-taj"
    },
    "SENTRY": {
        "ENABLED": True,
        "ENVIRONMENT": "alpha2",
        "DSN": "https://eda272d29c0540d896fe3a9edd7e1573@sentry.livspace.com/77"
    }
}

GATEWAY = {
    "HOST": "api.alpha2.livspace.com",
    "HEADERS": {
        "Authorization": "Basic U3Rhck1TRmUtRzREc0tJOjJTWGhMeEc3cHJhYmtXbDJySFFwdGJwSXl0OFhIOXln",
        "Content-Type": "application/json"
    }
}

EXTERNAL_SERVICES = {
    "LAUNCHPAD": {
        "HOST": "launchpad-backend.alpha2.livspace.com",
        "HEADERS": {
            "Content-Type": "application/json"
        },
        "GATEWAY": {
            "ENABLED": False,
            "PATH": '/launchpad'
        }
    },
    'BOUNCER': {
        'HOST': 'api.alpha2.livspace.com/bouncer',
        'HEADERS': {
            "Authorization": "Basic U3Rhck1TRmUtRzREc0tJOjJTWGhMeEc3cHJhYmtXbDJySFFwdGJwSXl0OFhIOXln",
            "X-Requested-By": "0",
            "Content-Type": "application/json"
        },
        "GATEWAY": {
            "ENABLED": True,
            "PATH": '/bouncer'
        }
    }
}
