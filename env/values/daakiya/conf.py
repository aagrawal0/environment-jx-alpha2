CONFIG = {
    "ENV": "alpha2",
    "DJANGO_LOG_LEVEL": "INFO",
    "DATABASE": {
        "HOST": "livspace-db",
        "USERNAME": "livspace",
        "PASSWORD": "Livspace123Stage",
        "DB_NAME": "livspace_v2",
        "PORT": "3306",
    },
    "REDIS": {
        'HOST': 'redis-webcms',
        'PORT': '26379',
        'MASTER': 'webcms-master',
        'KEY_PREFIX': 'alpha2-daakiya'
    },
    "GOOGLE_PLACES": {
        "API_KEY": "AIzaSyBxHsZxb5K3GBtqS9Bcr84-8rRjiACoyIk"
    },
    "EVENT": {
        "ENV": "local"
    },
    "DEBUG": False,
    "ALLOWED_HOSTS": ["*"],
    "SOLR": {
        "HOST": "http://ec2-52-77-254-6.ap-southeast-1.compute.amazonaws.com:8080/solr"
    },
    "AXLE": {
        "HOST": "http://axle.alpha2.livspace.com/",
        "CLIENT_ID": "Platform-Stage",
        "CLIENT_SECRET": "q12qKlQeVkPZZnuXnhXkZe0rMjn5ERXD"
    },
    "APM": {
        "INDEX_NAME": "daakiya-alpha2"
    }
}
